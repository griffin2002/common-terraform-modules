This is a small sample of some of the Modules I have created during
my previous employment as a DevOps Engineer
Most Modules include a README.md with Markdown to describe all variables and generally there is a pre-commit configuration that is run prior to commit to ensure the changes to the variables are recorded and values captured to facilitate documentation.

The included module is incomplete intentionally so as it will not be used in the wild; however, there are a few modifications that can be made to make it functional.
