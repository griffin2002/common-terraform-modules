<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13.1 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.72 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 3.72 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_autoscaling_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_policy) | resource |
| [aws_default_tags.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/default_tags) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_adjustment_type"></a> [adjustment\_type](#input\_adjustment\_type) | n/a | `any` | n/a | yes |
| <a name="input_autoscaling_group_name"></a> [autoscaling\_group\_name](#input\_autoscaling\_group\_name) | Name of the Autoscaling Group | `string` | n/a | yes |
| <a name="input_create"></a> [create](#input\_create) | Determines whether to create autoscaling group or not | `bool` | `true` | no |
| <a name="input_create_scaling_policy"></a> [create\_scaling\_policy](#input\_create\_scaling\_policy) | Determines whether to create target scaling policy schedule or not | `bool` | `true` | no |
| <a name="input_enabled"></a> [enabled](#input\_enabled) | Whether the scaling policy is enabled or disabled. | `bool` | n/a | yes |
| <a name="input_estimated_instance_warmup"></a> [estimated\_instance\_warmup](#input\_estimated\_instance\_warmup) | Estimated time, in seconds, until a newly launched instance will contribute CloudWatch metrics. | `number` | `null` | no |
| <a name="input_name"></a> [name](#input\_name) | Name of the policy | `string` | n/a | yes |
| <a name="input_policy_type"></a> [policy\_type](#input\_policy\_type) | Policy type, either 'SimpleScaling', 'StepScaling', 'TargetTrackingScaling', or 'PredictiveScaling'. | `string` | `"SimpleScaling"` | no |
| <a name="input_predictive_scaling_configuration"></a> [predictive\_scaling\_configuration](#input\_predictive\_scaling\_configuration) | Predictive scaling policy configuration to use with Amazon EC2 Auto Scaling. | <pre>object({<br>        max_capacity_breach_behavior = string<br>        max_capacity_buffer = number<br>        metric_specification = string<br>        mode = string<br>        scheduling_buffer_time = number<br>    })</pre> | n/a | yes |
| <a name="input_scaling_policies"></a> [scaling\_policies](#input\_scaling\_policies) | Map of target scaling policy schedule to create | `any` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_autoscaling_policy_arns"></a> [autoscaling\_policy\_arns](#output\_autoscaling\_policy\_arns) | ARNs of autoscaling policies |
<!-- END_TF_DOCS -->