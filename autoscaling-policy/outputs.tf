output "autoscaling_policy_arns" {
  description = "ARNs of autoscaling policies"
  value       = { for k, v in aws_autoscaling_policy.this : k => v.arn }
}