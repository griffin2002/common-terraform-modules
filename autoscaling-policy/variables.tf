variable "create" {
  description = "Determines whether to create autoscaling group or not"
  type        = bool
  default     = true
}
variable "create_scaling_policy" {
  description = "Determines whether to create target scaling policy schedule or not"
  type        = bool
  default     = true
}

variable "scaling_policies" {
  description = "Map of target scaling policy schedule to create"
  type        = any
  default     = {}
}

variable "adjustment_type" {

}
variable "name" {
    type = string
    description = "Name of the policy"
}

variable "autoscaling_group_name" {
  type = string
  description = "Name of the Autoscaling Group"
}

variable "policy_type" {
  description = "Policy type, either 'SimpleScaling', 'StepScaling', 'TargetTrackingScaling', or 'PredictiveScaling'."
  default = "SimpleScaling"
  type = string
  validation {
    condition = var.policy_type == "SimpleScaling" || var.policy_type == "StepScaling" || var.policy_type == "TargetTrackingScaling" || var.policy_type == "PredictiveScaling"
    error_message = "The value for policy type can only be one of the following: SimpleScaling (default), StepScaling, TargetTrackingScaling, or PredictiveScaling"
  }
}
variable "predictive_scaling_configuration" {
    description = "Predictive scaling policy configuration to use with Amazon EC2 Auto Scaling."
    type = object({
        max_capacity_breach_behavior = string
        max_capacity_buffer = number
        metric_specification = string
        mode = string
        scheduling_buffer_time = number
    })
  
}
variable "estimated_instance_warmup" {
  type = number
  default = null
  description = "Estimated time, in seconds, until a newly launched instance will contribute CloudWatch metrics."
}
variable "enabled" {
    type = bool
    description = "Whether the scaling policy is enabled or disabled."
  
}